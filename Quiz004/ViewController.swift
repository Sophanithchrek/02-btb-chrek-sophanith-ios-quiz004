//
//  ViewController.swift
//  Quiz004
//
//  Created by SOPHANITH CHREK on 9/12/20.
//

import UIKit

class ViewController: UIViewController {
    
    var user = [User]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url: URL = URL(string: "https://reqres.in/api/users?page=2")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if let err = error { print(err) }
            guard let data = data else { return }
            do {
                let response = try JSONDecoder().decode(Response.self, from: data)
                for rec in response.data {
                    self.user.append(contentsOf: [rec])
                }
            } catch let error {
                print(error)
            }
        }.resume()
        tableView.dataSource = self
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.user.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = "\(user[indexPath.row].first_name) : \(user[indexPath.row].last_name)"
        cell.detailTextLabel?.text = user[indexPath.row].email
        
        let url = URL(string: "\(user[indexPath.row].avatar)")
        let data = try? Data(contentsOf: url!)
        let myImage = UIImage(data: data!)
        cell.imageView?.image = myImage
        return cell
    }
    
}
