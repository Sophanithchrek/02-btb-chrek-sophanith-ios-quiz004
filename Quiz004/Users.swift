//
//  Users.swift
//  Quiz004
//
//  Created by SOPHANITH CHREK on 9/12/20.
//

import Foundation

struct User: Codable {
    var first_name: String
    var last_name: String
    var email: String
    var avatar: String
}

struct Response: Codable {
    var data: [User]
}
